import React from "react";
import { useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import { localServ } from "../services/localService";

export default function UserNav() {
  let user = useSelector((state) => {
    return state.userReducer.userInfo;
  });

  let handleLogout = () => {
    // Xoa data tu localStorage
    localServ.user.remove();
    // Remove data tu redux
    // dispatch({

    // })
    window.location.href = "/login";
  };

  let renderContent = () => {
    if (user) {
      // console.log("true");
      return (
        <>
          <span className="font-medium text-blue-500 underline">
            {user.hoTen}
          </span>
          <button
            onClick={() => {
              handleLogout();
            }}
            className="border rounded border-red-500 px-5 py-2"
          >
            Đăng xuất
          </button>
        </>
      );
    } else {
      return (
        <>
          <NavLink to="/login">
            {" "}
            <button className="border rounded border-black px-5 py-2 text-black">
              Đăng nhập
            </button>
          </NavLink>
          <button className="border rounded border-red-500 px-5 py-2">
            Đăng ký
          </button>
        </>
      );
    }
  };
  // console.log("user ", user);
  return <div className="space-x-5">{renderContent()}</div>;
}
