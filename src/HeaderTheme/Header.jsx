import React from "react";
import UserNav from "./UserNav";

export default function Header() {
  return (
    <div className="shadow">
      <div className="h-20 flex justify-between mx-auto items-center container">
        <span className="text-yellow-500 text-2xl font-medium">Cyber</span>
        <UserNav />
      </div>
    </div>
  );
}
