import { Col, Row } from "antd";
import React, { useEffect, useState } from "react";
import { movieServ } from "../../services/movicesService";
import ItemMovie from "./ItemMovie";
import TabsMovies from "./TabsMovies";

export default function HomePage() {
  const [movies, setMovies] = useState([]);
  useEffect(() => {
    movieServ
      .getListMovie()
      .then((res) => {
        // console.log(res);
        setMovies(res.data.content);
        // console.log("Movie ", movies);
      })
      .catch((err) => {
        // console.log(err);
      });
  }, []);

  const renderMovies = () => {
    return movies.map((data) => {
      return <ItemMovie data={data} key={data.maPhim} />;
    });
  };

  return (
    <div className="container mx-auto">
      <div class="grid grid-cols-5 gap-3">{renderMovies()}</div>
      <div>
        <TabsMovies />
      </div>
    </div>
  );
}
