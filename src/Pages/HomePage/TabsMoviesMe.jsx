import React from "react";
import { Tabs } from "antd";
import { useEffect } from "react";
import { movieServ } from "../../services/movicesService";
import { useState } from "react";

export default function TabsMovies() {
  const [dataMovies, setDataMovies] = useState([]);
  useEffect(() => {
    movieServ
      .getListMovie()
      .then((res) => {
        console.log(" danh sach phim cum rap", res.data.content);
        setDataMovies(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  let renderContent = () => {
    return dataMovies.map((heThongRap, index) => {
      let logo = heThongRap.logo;
      console.log("He thong logo", logo);
      return (
        <Tabs.TabPane
          tab={<img className="w-16 h-16" src={heThongRap.logo} />}
          key={index}
        ></Tabs.TabPane>
      );
    });
  };
  return (
    <div>
      <Tabs tabPosition="left" defaultActiveKey="1">
        {renderContent()}
      </Tabs>
    </div>
  );
}
