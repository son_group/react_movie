import React from "react";
import { Card } from "antd";
import { NavLink } from "react-router-dom";

const { Meta } = Card;

export default function ItemMovie({ data }) {
  return (
    <Card
      hoverable
      style={{
        width: "100%",
      }}
      cover={
        <img
          className="h-80 w-full object-cover"
          alt="example"
          src={data.hinhAnh}
        />
      }
    >
      <Meta
        title={
          <p className="text-red-500 truncate text-center">{data.tenPhim}</p>
        }
        description={data.biDanh}
      />
      <NavLink to={`/detail/${data.maPhim}`}>
        <button className="w-full py-2 bg-red-500 text-white mt-5 rouded hover:bg-black transition duration-300">
          Xem chi tiết
        </button>
      </NavLink>
    </Card>
  );
}
