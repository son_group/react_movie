import React, { useEffect, useState } from "react";
import { Tabs } from "antd";
import { movieServ } from "../../services/movicesService";
import ItemTabsMovie from "./ItemTabsMovie";

export default function TabsMovies() {
  const [dataMovies, setDataMovies] = useState([]);
  useEffect(() => {
    movieServ
      .getMovieByTheater()
      .then((res) => {
        // console.log(res);
        setDataMovies(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  let renderContent = () => {
    return dataMovies.map((heThongRap, index) => {
      return (
        <Tabs.TabPane
          tab={<img className="w-16 h-16" src={heThongRap.logo} />}
          key={index}
        >
          <Tabs style={{ height: 500 }} tabPosition="left">
            {heThongRap.lstCumRap.map((cumRap, index) => {
              return (
                <Tabs.TabPane
                  tab={
                    <div className="w-48 text-left ">
                      <p className="text-gray-700 truncate">
                        {cumRap.tenCumRap}
                      </p>
                      <p className="truncate">{cumRap.diaChi}</p>
                    </div>
                  }
                  key={index}
                >
                  {cumRap.danhSachPhim.map((phim) => {
                    // console.log("phim ", phim);
                    return (
                      <div>
                        <ItemTabsMovie data={phim} />
                      </div>
                    );
                  })}
                </Tabs.TabPane>
              );
            })}
          </Tabs>
        </Tabs.TabPane>
      );
    });
  };

  return (
    <div>
      <Tabs style={{ height: 500 }} tabPosition="left" defaultActiveKey="1">
        {renderContent()}
      </Tabs>
    </div>
  );
}
